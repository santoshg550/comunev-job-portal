import mongoose from 'mongoose'

const Schema = mongoose.Schema

interface IApplicant {
    name: string,
    email: string,
    position: string,
    resume: string,
    highestQualification: string
}

const applicant = new Schema({
    name: String,
    email: { type: String, unique: true },
    position: String,
    resume: String,
    highestQualification: String
}, {
    timestamps: true
})

const Applicant = mongoose.model('Applicant', applicant)

// Index creation may fail if duplicate records are already present. This
// may happen during development.
Applicant.on('index', function(err) {
    if(err) throw err
})

export { Applicant, IApplicant }
