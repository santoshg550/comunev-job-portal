'use strict'

import express from 'express'

import applicant from '../controllers/applicant'
import util from '../lib/util'

import * as validator from './validators'

const router = express.Router()

router.get('/form', (req, res, next) => {
    res.render('application', {
        title: 'Application Form'
    })
})

router.post('/submit', async (req, res, next) => {
    if(req.file)
        req.body.resume = req.file.path
    else
        req.body.resume = req.body.resumeLink

    let resp: any

    const errors = await validator.application(req.body)
    if(errors)
        resp = errors
    else {
        resp = await applicant.apply(req.body)
        res.status(201)
    }

    util.respond(resp, res, next)
})

export default router
