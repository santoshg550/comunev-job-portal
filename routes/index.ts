import express from 'express'

const router = express.Router()

router.get('/', (req, res, next) => {
    res.send('This is home')
})

export default router
