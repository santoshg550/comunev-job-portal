import { Validator } from 'node-input-validator'

import errors from '../lib/errors'

async function application(form: object) {
    const v = new Validator(form, {
        name: 'required',
        position: 'required',
        email: 'required|email',
        resume: 'required',
        highestQualification: 'required'
    });

    const matched = await v.check()

    if(!matched)
        return new errors.ValidationError('Invalid form inputs', 422)
}

export {
    application
}
