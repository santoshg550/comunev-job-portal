import mongoose from 'mongoose'
import { EventEmitter } from 'events'

class Connector extends EventEmitter {
    readonly databaseURI: string
    public Ready: Promise<any>

    constructor(databaseURI: string) {
        super()
        this.databaseURI = databaseURI
        this.Ready = new Promise(async (res, rej) => {
            try {
                await mongoose.connect(this.databaseURI, {
                    // reconnectTries: Number.MAX_VALUE,
                    // reconnectInterval: 1000,

                    useUnifiedTopology: true,

                    useNewUrlParser: true
                })

                mongoose.connection.on('error', err => {
                    this.emit('error', err)
                })

                res()
            } catch(err) {
                rej(err)
            }
        })
    }

    async close() {
        await mongoose.connection.close()
    }
}

export default Connector
