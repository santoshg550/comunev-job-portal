import { Applicant, IApplicant } from '../../models/applicant'

async function createApplicant(applicant: IApplicant) {
    const newApplicant = new Applicant({
        ...applicant
    })

    await newApplicant.save()
}

async function removeApplicant(aid: string) {
    await Applicant.deleteOne({ _id: aid })
}

export default {
    createApplicant,
    removeApplicant
}
