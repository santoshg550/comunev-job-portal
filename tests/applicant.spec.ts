import * as fs from 'fs'

import chai = require('chai')
import chaiHttp = require('chai-http')

import { Applicant, IApplicant } from '../models/applicant'

const expect = chai.expect

chai.use(chaiHttp)

describe('author', function() {
    describe('registration', function() {
        let newApplicant: IApplicant

        before(function() {
            newApplicant = {
                name: 'John Doe',
                email: 'john@mi5.com',
                position: 'Node.js Developer',
                resume: '',
                highestQualification: 'BE'
            }
        })

        it('it should submit application with resume file', async function() {
            const res = await chai
                .request(this.server)
                .post('/applicant/submit/')
                .type('form')
                .field('name', newApplicant.name)
                .field('email', newApplicant.email)
                .field('position', newApplicant.position)
                .field('highestQualification', newApplicant.highestQualification)
                .attach('resumeFile', fs.readFileSync('tests/resume.pdf'), 'resume.pdf')

            expect(res.status).to.equal(201)
        })

        it('it should submit application with resume link', async function() {
            const res = await chai
                .request(this.server)
                .post('/applicant/submit/')
                .type('form')
                .field('name', newApplicant.name)
                .field('email', newApplicant.email)
                .field('position', newApplicant.position)
                .field('highestQualification', newApplicant.highestQualification)
                .field('resumeLink', 'http://www.example.com/resume.pdf')

            expect(res.status).to.equal(201)
        })

        it('it should not submit application', async function() {
            const res = await chai
                .request(this.server)
                .post('/applicant/submit/')
                .type('form')
                .field('name', newApplicant.name)
                .field('email', newApplicant.email)
                .field('position', newApplicant.position)

            expect(res.status).to.equal(422)
        })

        it('it should not allow files other than docx and pdf', async function() {
            const res = await chai
                .request(this.server)
                .post('/applicant/submit/')
                .type('form')
                .field('name', newApplicant.name)
                .field('email', newApplicant.email)
                .field('position', newApplicant.position)
                .field('highestQualification', newApplicant.highestQualification)
                .attach('resumeFile', fs.readFileSync('tests/vid.mp4'))

            expect(res.status).to.equal(422)
        })

        it('it should not allow file size greater than 3MB', async function() {
            const res = await chai
                .request(this.server)
                .post('/applicant/submit/')
                .type('form')
                .field('name', newApplicant.name)
                .field('email', newApplicant.email)
                .field('position', newApplicant.position)
                .field('highestQualification', newApplicant.highestQualification)
                .attach('resumeFile', fs.readFileSync('tests/big_resume.pdf'), 'resume.pdf')

            expect(res.status).to.equal(422)
        })

        afterEach(async function() {
            await Applicant.deleteOne({ email: newApplicant.email })
        })
    })
})
