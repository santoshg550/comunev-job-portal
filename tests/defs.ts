import * as http from 'http'

interface Server {
    request: http.IncomingMessage,
    response: http.ServerResponse
}

export default Server
