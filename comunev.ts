#!/usr/bin/env node

import http from 'http'
import Blocked from 'blocked'

import Connector from './connections'
import config from './config'

import app from './web'

Blocked(ms => {
    console.log('BLOCKED FOR ' + (ms | 0) + 'ms')
})

async function begin(callback?: Function) {
    async function shutdown(err?: Error) {
        if(err)
            console.log(err)

        try {
            await connector.close()
        } catch(err) {
            console.log(err)
        }

        if(server)
            server.close(() => {
                console.log('Server is shutting down')

                setTimeout(() => {
                    process.exit(err ? 1 : 0)
                }, 2000)
            })
    }

    process.on('SIGTERM', () => {
        console.log('SIGTERM received')
        shutdown()
    })

    process.on('SIGINT', () => {
        console.log('SIGINT received')
        shutdown()
    })

    function handleErrors(err: Error) {
        console.log(err)
    }

    process.on('uncaughtException', handleErrors)
    process.on('unhandledRejection', handleErrors)

    const connector = new Connector(config.databaseURI)

    console.log('Waiting for connections...')
    await connector.Ready
    connector.on('error', shutdown)

    const reqListener = app()
    const server = http.createServer(reqListener)
    server.on('error', shutdown)
    server.listen(config.port, () => {
        console.log('Server is ready to accept requests at port', config.port)

        if(callback)
            callback(reqListener)
    })
}

export default begin

if(require.main == module)
    begin()
