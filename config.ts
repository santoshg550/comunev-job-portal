interface IConfig {
    readonly nodeEnv: string
    readonly port: number
    readonly databaseURI: string
}

[
    'NODE_ENV',
    'PORT',
    'DATABASE_URI'
].forEach((name) => {
    if(!process.env[name])
        throw new Error(`Environment variable ${name} is missing`)
})

const config: IConfig = {
    nodeEnv: process.env.NODE_ENV!,
    port: Number(process.env.PORT),
    databaseURI: process.env.DATABASE_URI!
}

export default config
