class ProjectError extends Error {
    status: number | undefined

    constructor(message: string, status?: number) {
        super(message)
        this.name = 'ProjectError'
        this.status = status
    }
}

class DatabaseError extends ProjectError {
    constructor(message: string ='Error requesting database') {
        super(message)
        this.name = 'DatabaseError'
    }
}

class ServerError extends ProjectError {
    constructor(message: string) {
        super(message)
        this.name = 'ServerError'
    }
}

class UserError extends ProjectError {
    constructor(message: string, status: number) {
        super(message, status)
        this.name = 'UserError'
    }
}

class ValidationError extends ProjectError {
    constructor(message: string, status: number) {
        super(message, status)
        this.name = 'ValidationError'
    }
}

export default {
    ProjectError,
    ServerError,
    DatabaseError,
    UserError,
    ValidationError
}
