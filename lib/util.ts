interface Resp {
    json: Function
}

interface Next {
    (obj: Error): void
}

function respond(data: object, res: Resp, next: Next) {
    if(data instanceof Error)
        return next(data)

    res.json(data)
}

export default {
    respond
}
