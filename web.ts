import path from 'path'

import express, { Request, Response, NextFunction } from 'express'
import multer from 'multer'
import helmet from 'helmet'
import compression from 'compression'
import bodyParser from 'body-parser'

import index from './routes/index'
import applicant from './routes/applicant'

import errors from './lib/errors'

export default function() {
    const app = express()

    const upload = multer({
        dest: 'uploads/',
        fileFilter: function (req, file, callback) {
            const ext = path.extname(file.originalname)

            if(ext !== '.docx' && ext !== '.pdf')
                return callback(new errors.ValidationError('Only docx or pdf are allowed', 422))

            callback(null, true)
        },
        limits: { fileSize: 3145728 }
    })

    app
        .set('view engine', 'ejs')
        .use(compression())
        .use(express.static('public'))
        .use(bodyParser.json())
        .use(helmet())
        .use(helmet.contentSecurityPolicy({
            directives: {
                defaultSrc: ["'self'"],
                scriptSrc: [
                    "'self'",
                    "'unsafe-eval'",
                    'ajax.googleapis.com',
                    'maxcdn.bootstrapcdn.com',
                    'code.jquery.com'
                ],
                styleSrc: [
                    "'self'",
                    "'unsafe-inline'",
                    'ajax.googleapis.com',
                    'maxcdn.bootstrapcdn.com',
                    'cdnjs.cloudflare.com',
                    'code.jquery.com'
                ],
                imgSrc: [
                    "'self'",
                    'code.jquery.com'
                ],
                fontSrc: [
                    'cdnjs.cloudflare.com',
                    'maxcdn.bootstrapcdn.com'
                ],
                connectSrc: ["'self'"]
            },
            setAllHeaders: true
        }))
        .use(helmet.noCache())
        .disable('x-powered-by')

    app
        .use('/', index)
        .use('/applicant', upload.single('resumeFile'), applicant)
        .use((req: Request, res: Response, next: NextFunction) => {
            next({
                status: 404,
                message: `No such route ${req.path}`
            })
        })
        .use((err: any, req: Request, res: Response, next: NextFunction) => {
            if(err.code === 'LIMIT_FILE_SIZE')
                err.status = 422

            res.status(err.status || 500).json({ message: err.message })
        })

    return app
}
