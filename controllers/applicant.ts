import error from '../lib/errors'

import { Applicant, IApplicant } from '../models/applicant'

async function apply(applicant: IApplicant) {
    try {
        const newApplicant = new Applicant(applicant)

        await newApplicant.save()
    } catch(err) {
        if(err.code == 11000)
            return new error.UserError('Duplicate email', 403)

        return new error.DatabaseError('Error saving info')
    }

    return { message: 'Applicant registered successfully' }
}

export default {
    apply
}
