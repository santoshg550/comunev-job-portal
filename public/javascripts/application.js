$(document).ready(function() {
    const form = $('#jobApplication')

    const resumeFile = $("input[name=resumeFile]")
    const resumeLink = $("input[name=resumeLink]")

    resumeFile.change(function() {
        const resume = this.files[0]

        if(!resume)
            return

        if(resume.size > 3145728) {
            this.value = null
            return alert('File too big')
        }

        resumeLink.val('')

        // var ext = resume.name.split('.').pop().toLowerCase()
        // if(!/docx|pdf/.test(ext)) {
        //     this.value = null
        //     return alert('Only docx or pdf files are allowed')
        // }
    })

    resumeLink.keydown(function() {
        resumeFile.val('')
    })

    form.submit(function(ev) {
        ev.preventDefault()

        const formData = new FormData($(this)[0])

        if(!formData.get('resumeFile').name && !formData.get('resumeLink'))
            return alert('Resume is needed')

        $.ajax({
            url: '/applicant/submit',
            type: 'POST',
            enctype: 'multipart/form-data',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data, status, jqXHR) {
                if(status == "success")
                    alert(data.message)
            },
            error: function(jqXHR, status, err) {
                alert(JSON.parse(jqXHR.responseText).message)
            }
        })
    })
})
